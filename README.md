Licensed under GNU GPLv3.

Project written between the 25-30 August 2019.
It is a Picross solver written in Python which internally uses a SAT solver (parallelised glucose).
The goal of the project and its difficulty resides in translating the problem in SAT form and in a way that allows it to be efficiently solved using current algorithms.

It can currently solve grids of size up to 33x33 of density 0.5 (medium difficulty) in approximately 15 minutes.
For comparison, another project that worked on the same problem could only solve grids up to 5x5 and sometimes 6x6 (of medium complexity).

We implement several methods which can be found in writer_2.py, writer_3.py and writer_thread.py.

We have added parallelism, terminal colorized output.

We did not have the time to implement another algorithm that could - in a perfect scenario - allow us to solve grids of size approx. 2x bigger. Instead of generating all possible configurations of crosses on a line, we could generate the configurations obtained by skipping a cross every two crosses and adding other conditions to compensate but effectively bringing us to a grid 2x smaller.
For further improvement, we could better manage IO by writing the generated SAT formula in a stream-like fashion instead of bulk-writing when the generation is done ( it is a major bottleneck for sizes 30 and up - several Gb of formula); and then parallelize the generation of the formula.

Instructions to generate a board (script boardgen.py found on the Internet):

rm taai11-b-question.txt && python boardgen.py 9 1 0.5 0.4 123 && python solver.py taai11-b-question.txt

