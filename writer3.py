from multiprocessing import Pool, TimeoutError, Value

CHAR_RED, CHAR_END = '\033[91m','\033[0m' 
CHAR_GRN = "\033[92m"

def comb(n,k):
	if k > n:
		return 0
	if k == 0:
		return 1
	m = 1
	for i in range(k):
		m *= (n-i)
	for i in range(1,k+1):
		m //= i
	return m

def n_maj_yvars_used(n, cond):
	k = n+1
	for m in cond:
		k -= m
	return comb(k-1, len(cond)-1)

def id_var(n, l_or_c, other):
	if l_or_c < n: # col
		l,c = other, l_or_c 
		return l*n + c + 1
	l,c = l_or_c - n, other # line
	return l*n + c + 1

yid = Value("i", 0, lock=True)

def apply_block(config, i_start, block_size, val = 1):
	for i_square in range(block_size):
		config[i_start + i_square] = val

def gen_zj_wij(n, line_or_col, cond, i_z):
	clauses, n_clauses = "",0
	block = cond[-1]
	first = 0

	# enlever premiers indices
	for i in range(len(cond)-1):
		first += cond[i] + 1

	p = n+1-block
	i_w = i_z + n
	
	for z in range(first, p):
		# nouveau debut
		for j in range(z,n):
			clauses += "-" + str(i_w + z*n+z) + " "
			if j >= z+block:
				clauses += "-"
			clauses += str(id_var(n, line_or_col, j)) + " 0\n"
			n_clauses += 1
			
		# generer les configurations pour chaque debut de bloc
		for w in range(z+1, p):
			# clauses += "c i={} z={} w\n".format(line_or_col, i_z+z)
			clauses += "-" + str(i_w+z*n+w) + " " + str(i_w+(z+1)*n+w) + " 0\n"
			clauses += "-" + str(i_w+z*n+w) + " -" + str(id_var(n, line_or_col, z)) + " 0\n"
			n_clauses += 2
		# au moins une config
		# clauses += "c i={} z >=1\n".format(line_or_col)
		clauses += "-" + str(i_z + z) + " "
		for w in range(z, p):
			clauses += str(i_w + z*n + w) + " "
		clauses += "0\n"
		n_clauses += 1

	return clauses, n_clauses

def gen_configs_line_or_col(i_y, i_z, line_or_col, config, i, blocks):
	n = len(config)

	if i >= n:
		return "",0,i_y

	if blocks == []:
		raise Exception("ERROR")
	clauses, n_clauses = "", 0
	block_size = blocks[0]

	if i + block_size-1 >= n:
		return "",0, i_y

	if len(blocks) == 1:
		
		# on s'arrete a l'avant dernier bloc
		#clauses += "c i={} y={}: gen conf ligne\n".format(line_or_col, i_y)
		for j in range(i):
			clauses += "-" + str(i_y) + " "
			if not config[j]:
				clauses += "-"
			clauses += str(id_var(n, line_or_col, j)) + " 0\n"

		# dernier bloc: on veut un des z_i
		#clauses += "c i={} y={}: dernier block z_i\n".format(line_or_col, i_y)
		clauses += "-" + str(i_y) + " " + str(i_z + i) + " 0\n"

		return clauses,i+1,i_y+1	


	for i_start_block in range(i, n - block_size + 1): # exclus
		# pas besoin de verifier que ca ne depasse pas n
		apply_block(config, i_start_block, block_size, 1)

		# on saute une ligne d'ou le +1 et on retire le dernier element de la liste

		r,c,v = gen_configs_line_or_col(i_y, i_z, line_or_col, config, i_start_block + block_size + 1, blocks[1:]) 
		clauses += r
		n_clauses += c
		i_y += v-i_y # int immutable

		# on annule l'application du bloc
		apply_block(config, i_start_block, block_size, 0)
	return clauses, n_clauses,i_y


def read_blocks_file(input_name):
	"""
	- Prends le nom du fichier contenant les conditions
	- Renvoie les conditions: un tableau de 2*n cases qui stockent les blocs à placer sur chaque colonne (les n premieres cases) puis chaque ligne (les n derniers). Ces cases sont donc des tableaux de nombres
	"""

	conditions = []
	with open(input_name) as input_file:
		unique = False
		loc_conditions = []
		for line in input_file:
			line = line.replace("\n","").split("\t")
			#print(line)
			#print(loc_conditions)

			if line[0].startswith("$"): #ne prendre que le premier picross
				if unique:
					conditions.append(list(loc_conditions))
					loc_conditions = []
					continue
				unique = True
				continue
			if line == ['']: # si ligne ou colonne sans contrainte
				loc_conditions.append([])
				continue
			
			loc_conditions.append([])
			for bloc in line:
				loc_conditions[-1].append(int(bloc))
				
		if loc_conditions != []:
			conditions.append(list(loc_conditions))

	return conditions

'''
clause: classe sympy compose de ou
'''

class Naive3MultiWriter:
	def __init__(self):
		self.clauses, self.n_clauses, self.n_vars = [],[],[]

	def gen_conf_thread(self, line_or_col, i):
		n = self.n[i]
		config = n*[0]
		
		cond = self.conditions[i][line_or_col]
		if cond == []:
			clauses = ""
			for j in range(n):
				clauses += "-" + str(id_var(n, line_or_col, j)) + " 0\n"
			return line_or_col,clauses, n
		
		n_yvars = n_maj_yvars_used(n, cond)
		i_y = 0

		with yid.get_lock():
			i_y += yid.value
			yid.value += n_yvars+n+n*n

		i_z = i_y + n_yvars
		# print(yid.value, i_z, i_y)
		clauses, n_clauses,i_final = gen_configs_line_or_col(i_y, i_z, line_or_col, config, 0, cond)

		# zj link with xij
		c,v = gen_zj_wij(n, line_or_col, cond, i_z)
		clauses += c
		n_clauses += v

		assert(n_yvars >= i_final -i_y)
		n_yvars = i_final - i_y
			
		# at least y_i
		#clauses += "c i={} >=1 y\n".format(line_or_col)
		for y_i in range(n_yvars):
			clauses += str(i_y + y_i) + " "
		clauses += "0\n"		
		n_clauses += 1


		print("{}[{}]{}({})".format(CHAR_GRN,line_or_col, CHAR_END,n_clauses), end=" ", flush = True)
		return line_or_col,clauses, n_clauses

	def read_picross(self, filename):
		self.conditions = read_blocks_file(filename)
		# TODO: assert...
		self.n = [len(self.conditions[i])//2 for i in range(len(self.conditions))]
		
		print("Chargement de", len(self.conditions), "nonogrammes")

	def write_dimacs(self, filename, i):
		n = self.n[i]
		n_vars = self.n_vars[i]
		n_clauses = self.n_clauses[i]

		result = "p cnf " + str(n_vars) + " " + str(n_clauses) + "\n"
		result += self.clauses[i]
	
		# write to file 
		with open(filename, "w") as input_fd:
			input_fd.write(result)
		#print(result)

	def gen_clauses(self, i):
		self.clauses.append("")
		self.n_clauses.append(0)
		self.n_vars.append(0)
			
		print("{}Generation:{}".format(CHAR_RED,CHAR_END), end=" ")
		
		print(self.conditions[i])
	
		global yid
		with Pool() as p:
			n = self.n[i]
			m = 2*n
               
			with yid.get_lock():         
				yid.value = n*n+1
			args = [(j, i) for j in range (m)]
			
			resultats = p.starmap(self.gen_conf_thread, args)
			p.close()
			p.join()
			
			v = [[] for j in range(m)]
			
			for j, clauses, n_clauses in resultats:
				self.clauses[i] += clauses
				self.n_clauses[i] += n_clauses

			with yid.get_lock():         
				self.n_vars[i] = yid.value

			# PAS BESOIN d'IMPOSER L'UNICITE
		print("{}[DONE]{}".format(CHAR_RED,CHAR_END))
