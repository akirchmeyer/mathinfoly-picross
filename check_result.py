
def get_count(line):
    blocks = []
    in_block = 0
    for var in line:
        if not '-' in var:
            in_block += 1
        else:
            if in_block > 0: # Fin de block
                blocks.append(in_block)
                in_block = 0
    if in_block > 0:
        blocks.append(in_block)
    return blocks
            
def check_lines(result, line_conditions):
    width = len(line_conditions)
    return all(line_conditions[line_nb] == get_count(result[line_nb * width : (line_nb+1) * width]) for line_nb in range(width))

def check_columns(result, columns_conditions):
    height = len(columns_conditions)
    
    values = [result[col_nb * height + line_nb] for line_nb in range(height) for col_nb in range(height)]
    return all(columns_conditions[col_nb] == get_count(values[col_nb * height : (col_nb+1) * height]) for col_nb in range(height))

        
def check_result(result, conditions):
    result = result.strip().split(" ")
    width = len(conditions)//2
    if check_lines(result, conditions[width:]) and check_columns(result, conditions[:width]):
        print("Valide")
        return True
    else:
        print("Invalide")
        return False

